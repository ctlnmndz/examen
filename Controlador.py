#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Controlador():

    def __init__(self):
        self.estado = 0

    # Funcion para prestra libros.
    def prestar(self, documento):
        estado = int(input("¿Desea realizar el prestamo?, si"
                           " su respuesta es si presione 1: "))
        # Si desea llevarselo
        if(estado == 1):
            documento.set_estado("Prestado")
            print("El prestamo se ha realizado con exito")
            print(documento.get_titulo(), " se encuentra ",
                  documento.get_estado())
        # Si no desea llevarselo
        else:
            print("\nSi quiere puede revisar otro libro o revista")

    # Función para entregar libros
    def entregar(self, documento):
        # Se analiza si estaba prestado
        if(documento.get_estado() == "Prestado"):
            documento.set_estado("Disponible")
            print("La devolución se ha realizado con exito")
            print(documento.get_titulo(), " se encuentra ",
                  documento.get_estado())
        # Si no estaba prestado
        else:
            print(documento.get_titulo(), "no ha sido prestado")