#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Se importa la clase infocomun
from Infocomun import Infocomun


class Revista(Infocomun):

    # Función que guarda los datos por herencia
    def __init__(self, titulo, anio, codigo):
        super().__init__(titulo, anio, codigo)
        edicion = int(input("ingrese el numero de edición de la revista: "))
        self.__edicion = edicion

    def get_edicion(self,):
        return self.__edicion