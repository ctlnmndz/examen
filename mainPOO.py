#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Se importan las clases
from Libro import Libro
from Revista import Revista
from Controlador import Controlador


# Función para crear los libros
def creacion_libros(titulo, anio, codigo):
    # Se le dan los paramatros
    titulo1 = titulo
    anio1 = anio
    codigo1 = codigo

    # Se llama importa la clase libro
    libro = Libro(titulo1, anio1, codigo1)
    return libro


# Funcion para las revistas
def crear_revista():
    # Se piden datos
    titulo2 = str(input("Ingrese el titulo de la revista: "))
    anio2 = int(input("Ingrese el año de publicación de la revista: "))
    codigo2 = int(input("Ingrese el código de la revista: "))

    # Se importa la clase revista
    revista = Revista(titulo2, anio2, codigo2)
    return revista


# Función que organiza los datos
def main():

    # Se importa la clase Controlador
    bibliotecaria = Controlador()
    print("¿Por qué desea consultar?")
    eleccion = int(input("Si es por un libro indique 1,"
                         " si es por revista indique 2: "))

    # Se elige el libro
    if(eleccion == 1):
        libro1 = creacion_libros("Romeo y Julieta", 2004, 98785)
        libro2 = creacion_libros("El principito", 2008, 45321)
        libro3 = creacion_libros("Cien años de soledad", 1996, 65982)

        print("\nLibros disponible en nuestra biblioteca:")
        libro1.mostrar()
        print("Estado: ", libro1.get_estado(), "\n")

        libro2.mostrar()
        print("Estado: ", libro2.get_estado(), "\n")

        libro3.mostrar()
        print("Estado: ", libro3.get_estado(), "\n")

        opcion = int(input("Ingrese la opción 1, 2 o 3: "))

        # Se llama las funciones para dar el parametro de controlador
        if(opcion == 1):
            bibliotecaria.prestar(libro1)
        elif(opcion == 2):
            bibliotecaria.prestar(libro2)
        elif(opcion == 3):
            bibliotecaria.prestar(libro3)

    # Se elige revista
    elif(eleccion == 2):
        revista1 = crear_revista()
        print("\nInformación de la revista")
        revista1.mostrar()
        print("Número de edición: ", revista1.get_edicion())

    print("¿Desea buscar otro libro o revista?\n")
    print("¿O nesecita hacer una entrega de un libro?")

    # Se pregunta si quiere seguir en el programa.
    seguir = int(input("Si desea buscar presione 1\n"
                       "Si desea hacer una entrega presione 2\n"
                       "De lo contario presione 0\n"))

    # Si sigue y desea elegir se vuelve a llamar la función.
    if(seguir == 1):
        main()

    # Si sigue y desea entregar, se pregunta cual quiere entregar.
    if(seguir == 2):
        print("¿Qué libro desea entregar?")
        entrega = int(input("¿1, 2 o 3?"))

        # Se entregan y vuelve a llamar la función main.
        if(entrega == 1):
            bibliotecaria.entregar(libro1)
            main()

        if(entrega == 2):
            bibliotecaria.entregar(libro2)
            main()

        if(entrega == 3):
            bibliotecaria.entregar(libro3)
            main()

    # Si no quiere seguir se termina el progrma.
    else:
        print("Gracias por solicitar de nuestros servicios")


if __name__ == '__main__':

    # Se llama la funcion para iniciar el programa
    print("***Bienvenidos al sistema de bibliotecas 2020***")
    main()