#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Se importa la clase infocomun
from Infocomun import Infocomun


class Libro(Infocomun):
    # Se dan los datos por herencia
    def __init__(self, titulo, anio, codigo):
        super().__init__(titulo, anio, codigo)
        self.__estado = "Disponible"

    def set_estado(self, estado):
        self.__estado = estado

    def get_estado(self):
        return self.__estado