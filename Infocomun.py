#!/usr/bin/env python
# -*- coding: utf-8 -*-


# clase de la información comun
class Infocomun():

    # Se crean las clases
    def __init__(self, titulo, anio, codigo):
        self.__titulo = titulo
        self.__anio = anio
        self.__codigo = codigo

    def get_titulo(self):
        return self.__titulo

    def get_anio(self):
        return self.__anio

    def get_codigo(self):
        return self.__codigo

    def mostrar(self):
        print("Título: ", self.get_titulo(),
              "\nCódigo: ", self.get_codigo(),
              "\nAño: ", self.get_anio())